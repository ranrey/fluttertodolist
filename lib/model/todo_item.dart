import 'package:floor/floor.dart';

@Entity(tableName: "ToDoItems")
class ToDoItem {

  @PrimaryKey(autoGenerate: true)
  int? id;

  final String descriptions;

  final bool isDone;

  ToDoItem(this.id, this.descriptions, this.isDone);
}