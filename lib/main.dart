import 'package:flutter/material.dart';
import 'package:flutter_todolist/app.dart';

import 'data/database.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final database =
      await $FloorAppDatabase.databaseBuilder('flutter_database.db').build();
  final dao = database.toDoItemDao;

  runApp(FlutterToDoListApp(dao: dao));
}
