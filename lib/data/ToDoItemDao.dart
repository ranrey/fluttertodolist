import 'package:floor/floor.dart';
import 'package:flutter_todolist/model/todo_item.dart';

@dao
abstract class ToDoItemDao {

  @Query('SELECT * FROM ToDoItems')
  Future<List<ToDoItem>> findAllToDoItems();

  @insert
  Future<void> insertToDoItem(ToDoItem toDoItem);

  @update
  Future<void> updateToDoItem(ToDoItem toDoItem);

  @Query('DELETE FROM ToDoItems')
  Future<void> deleteAllToDoItems();
}