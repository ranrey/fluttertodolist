import 'dart:async';

import 'package:floor/floor.dart';
import 'package:flutter_todolist/model/todo_item.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:path/path.dart';
import 'ToDoItemDao.dart';

part 'database.g.dart';

@Database(version: 1, entities: [ToDoItem])
abstract class AppDatabase extends FloorDatabase {
  ToDoItemDao get toDoItemDao;
}