// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  ToDoItemDao? _toDoItemDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback? callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `ToDoItems` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `descriptions` TEXT NOT NULL, `isDone` INTEGER NOT NULL)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  ToDoItemDao get toDoItemDao {
    return _toDoItemDaoInstance ??= _$ToDoItemDao(database, changeListener);
  }
}

class _$ToDoItemDao extends ToDoItemDao {
  _$ToDoItemDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _toDoItemInsertionAdapter = InsertionAdapter(
            database,
            'ToDoItems',
            (ToDoItem item) => <String, Object?>{
                  'id': item.id,
                  'descriptions': item.descriptions,
                  'isDone': item.isDone ? 1 : 0
                }),
        _toDoItemUpdateAdapter = UpdateAdapter(
            database,
            'ToDoItems',
            ['id'],
            (ToDoItem item) => <String, Object?>{
                  'id': item.id,
                  'descriptions': item.descriptions,
                  'isDone': item.isDone ? 1 : 0
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<ToDoItem> _toDoItemInsertionAdapter;

  final UpdateAdapter<ToDoItem> _toDoItemUpdateAdapter;

  @override
  Future<List<ToDoItem>> findAllToDoItems() async {
    return _queryAdapter.queryList('SELECT * FROM ToDoItems',
        mapper: (Map<String, Object?> row) => ToDoItem(row['id'] as int?,
            row['descriptions'] as String, (row['isDone'] as int) != 0));
  }

  @override
  Future<void> deleteAllToDoItems() async {
    await _queryAdapter.queryNoReturn('DELETE FROM ToDoItems');
  }

  @override
  Future<void> insertToDoItem(ToDoItem toDoItem) async {
    await _toDoItemInsertionAdapter.insert(toDoItem, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateToDoItem(ToDoItem toDoItem) async {
    await _toDoItemUpdateAdapter.update(toDoItem, OnConflictStrategy.abort);
  }
}
