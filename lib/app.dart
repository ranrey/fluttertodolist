import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_todolist/data/ToDoItemDao.dart';
import 'package:flutter_todolist/bloc/todo_list_bloc.dart';
import 'package:flutter_todolist/bloc/todo_list_event.dart';
import 'package:flutter_todolist/view/main_screen.dart';

class FlutterToDoListApp extends StatelessWidget {
  const FlutterToDoListApp({required this.dao, Key? key}) : super(key: key);
  final ToDoItemDao dao;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
            create: (_) => ChangeToDoListBloc(dao)),
        BlocProvider(
            create: (context) => ToDoListBloc(dao: dao, changeToDoListBloc: BlocProvider.of<ChangeToDoListBloc>(context))..add(const ToDoListLoaded())
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MainScreen(dao: dao),
      ),
    );
  }
}
