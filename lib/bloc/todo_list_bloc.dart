import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_todolist/data/ToDoItemDao.dart';
import 'package:flutter_todolist/bloc/todo_list_event.dart';
import 'package:flutter_todolist/bloc/todo_list_state.dart';

class ToDoListBloc extends Bloc<ToDoListEvent, ToDoListState> {
  final ToDoItemDao dao;
  final ChangeToDoListBloc changeToDoListBloc;
  late StreamSubscription changeToDoListSubscription;

  ToDoListBloc({required this.dao, required this.changeToDoListBloc}) : super(const ToDoListState()) {
    on<ToDoListLoaded>(_onLoaded);
    changeToDoListSubscription = changeToDoListBloc.stream.listen((event) {
      if (event.status == Status.success) {
        add(const ToDoListLoaded());
      }
    });
  }

  Future<void> _onLoaded(
      ToDoListLoaded event, Emitter<ToDoListState> emit) async {
    emit(state.copyWith(status: Status.initial));
    await dao
        .findAllToDoItems()
        .then((value) =>
            emit(state.copyWith(status: Status.success, toDoItems: value)))
        .onError((error, stackTrace) =>
            emit(state.copyWith(status: Status.failure)));
  }
}

class ChangeToDoListBloc extends Bloc<ToDoListEvent, ChangeToDoListState> {
  final ToDoItemDao dao;

  ChangeToDoListBloc(this.dao) : super(const ChangeToDoListState()) {
    on<AddNewToDoItem>(_onAddNewToDoItem);
    on<UpdatedToDoItem>(_onUpdateToDoItem);
    on<ClearedAllToDoItem>(_onClearedToDoItem);
  }

  Future<void> _onAddNewToDoItem(AddNewToDoItem event, Emitter<ChangeToDoListState> emit) async {
    emit(state.copyWith(status: Status.initial, type: ChangeToDoListType.add));
    await dao
        .insertToDoItem(event.toDoItem)
        .then((_) => emit(state.copyWith(status: Status.success)))
        .onError((error, stackTrace) =>
            emit(state.copyWith(status: Status.failure)));
  }

  Future<void> _onUpdateToDoItem(UpdatedToDoItem event, Emitter<ChangeToDoListState> emit) async {
    emit(state.copyWith(status: Status.initial, type: ChangeToDoListType.update));
    await dao.updateToDoItem(event.toDoItem)
    .then((_) => emit(state.copyWith(status: Status.success)))
    .onError((error, stackTrace) => emit(state.copyWith(status: Status.failure)));
  }

  Future<void> _onClearedToDoItem(
      ClearedAllToDoItem event, Emitter<ChangeToDoListState> emit) async {
    emit(state.copyWith(status: Status.initial, type: ChangeToDoListType.clear));
    await dao.deleteAllToDoItems()
        .then((_) => emit(state.copyWith(status: Status.success)))
        .onError((error, stackTrace) => emit(state.copyWith(status: Status.failure)));
  }
}
