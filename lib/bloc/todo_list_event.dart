import 'package:equatable/equatable.dart';
import 'package:flutter_todolist/model/todo_item.dart';

abstract class ToDoListEvent extends Equatable {
  const ToDoListEvent();

  @override
  List<Object> get props => [];
}

class ToDoListLoaded extends ToDoListEvent {
  const ToDoListLoaded();
}

class AddNewToDoItem extends ToDoListEvent {
  final ToDoItem toDoItem;
  const AddNewToDoItem(this.toDoItem);
}

class UpdatedToDoItem extends ToDoListEvent {
  final ToDoItem toDoItem;
  const UpdatedToDoItem(this.toDoItem);
}

class ClearedAllToDoItem extends ToDoListEvent {
  const ClearedAllToDoItem();
}