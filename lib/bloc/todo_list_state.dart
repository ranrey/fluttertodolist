import 'package:equatable/equatable.dart';
import 'package:flutter_todolist/model/todo_item.dart';

enum Status { initial, success, failure }

class ToDoListState extends Equatable {
  final List<ToDoItem> toDoItems;
  final Status status;

  const ToDoListState(
      {this.toDoItems = const <ToDoItem>[], this.status = Status.initial});

  ToDoListState copyWith({
    List<ToDoItem>? toDoItems,
    Status? status,
  }) {
    return ToDoListState(
      toDoItems: toDoItems ?? this.toDoItems,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props => [toDoItems, status];
}

enum ChangeToDoListType { add, update, clear}

class ChangeToDoListState extends Equatable {
  final Status status;
  final ChangeToDoListType? type;

  const ChangeToDoListState({
    this.status = Status.initial,
    this.type
  });

  ChangeToDoListState copyWith({
    Status? status,
    ChangeToDoListType? type
  }) {
    return ChangeToDoListState(
      status: status ?? this.status,
      type: type ?? this.type,
    );
  }

  @override
  List<Object?> get props => [status, type];
}
