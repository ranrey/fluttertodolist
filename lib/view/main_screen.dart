import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_todolist/data/ToDoItemDao.dart';
import 'package:flutter_todolist/bloc/todo_list_bloc.dart';
import 'package:flutter_todolist/bloc/todo_list_event.dart';
import 'package:flutter_todolist/bloc/todo_list_state.dart';
import 'package:flutter_todolist/model/todo_item.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({required this.dao, Key? key}) : super(key: key);
  final ToDoItemDao dao;

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  var isTextFieldVisible = false;
  final _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: BlocProvider.of<ChangeToDoListBloc>(context),
      listener: _changeToDoListBlocListener,
      child: Scaffold(
          appBar: AppBar(
            title: const Text('Flutter ToDoList'),
          ),
          body: Column(
            children: [
              const Flexible(
                child: ToDoItemListWidget(),
              ),
              Container(
                padding: const EdgeInsets.all(8),
                child: isTextFieldVisible ? _textField() : _mainControls(),
              )
            ],
          )),
    );
  }

  void _changeToDoListBlocListener(BuildContext context, ChangeToDoListState state) {
    if (state.status == Status.success) {
      _showSuccessSnackBar(state, context);
    } else {
      _showFailedSnackBar(state, context);
    }
  }

  void _showFailedSnackBar(ChangeToDoListState state, BuildContext context) {
    if (state.status == Status.failure) {
      if (state.type == ChangeToDoListType.add) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Не удалось добавить новое дело')));
      }
      if (state.type == ChangeToDoListType.update) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Не удалось изменить статус дела')));
      }
      if (state.type == ChangeToDoListType.clear) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Не удалось очистить список дел')));
      }
    }
  }

  void _showSuccessSnackBar(ChangeToDoListState state, BuildContext context) {
    if (state.type == ChangeToDoListType.add) {
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Добавлено новое дело')));
    }
    if (state.type == ChangeToDoListType.update) {
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Статус дело был изменен')));
    }
    if (state.type == ChangeToDoListType.clear) {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Очистка выполнена')));
    }
  }

  Widget _mainControls() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: ElevatedButton(
              child: const Text("Добавить"),
              onPressed: () {
                setState(() {
                  isTextFieldVisible = true;
                });
              }),
        ),
        const SizedBox(
          width: 8,
        ),
        Expanded(
          child: ElevatedButton(
              onPressed: () {
                context
                    .read<ChangeToDoListBloc>()
                    .add(const ClearedAllToDoItem());
              },
              child: const Text('Очистить')),
        ),
      ],
    );
  }

  Widget _textField() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: TextField(
            controller: _textController,
            autofocus: true,
          ),
        ),
        const SizedBox(
          width: 8,
        ),
        ElevatedButton(
          onPressed: () async {
            var toDoItem = ToDoItem(null, _textController.text, false);
            context.read<ChangeToDoListBloc>().add(AddNewToDoItem(toDoItem));
            _textController.clear();
            setState(() {
              isTextFieldVisible = false;
            });
          },
          child: const Icon(Icons.add),
        ),
        const SizedBox(
          width: 4,
        ),
        ElevatedButton(
          onPressed: () {
            setState(() {
              isTextFieldVisible = false;
            });
          },
          child: const Icon(Icons.cancel),
        ),
      ],
    );
  }
}

class ToDoItemWidget extends StatelessWidget {
  const ToDoItemWidget({Key? key, required this.toDoItem}) : super(key: key);
  final ToDoItem toDoItem;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: [
          Checkbox(
              value: toDoItem.isDone,
              onChanged: (isChecked) {
                context.read<ChangeToDoListBloc>().add(UpdatedToDoItem(
                    ToDoItem(toDoItem.id, toDoItem.descriptions, isChecked!)));
              }),
          Text(
            toDoItem.descriptions,
            style: TextStyle(
                decoration: toDoItem.isDone
                    ? TextDecoration.lineThrough
                    : TextDecoration.none),
          )
        ],
      ),
    );
  }
}

class ToDoItemListWidget extends StatelessWidget {
  const ToDoItemListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ToDoListBloc, ToDoListState>(
        buildWhen: (previous, current) => previous != current,
        builder: (context, state) {
          if (state.status == Status.success) {
            if (state.toDoItems.isEmpty) {
              return const Center(
                child: Text("Пусто"),
              );
            } else {
              return ListView.builder(
                itemCount: state.toDoItems.length,
                itemBuilder: (_, index) {
                  return ToDoItemWidget(toDoItem: state.toDoItems[index]);
                },
              );
            }
          } else {
            if (state.status == Status.initial) {
              return const Center(
                child: Text("Загружается"),
              );
            } else {
              return const Center(
                child: Text("Ошибка"),
              );
            }
          }
        });
  }
}
